#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import sys
import logging
from datetime          import date

#from pickle           import load,dump
import json
import uuid

from tkinter           import Button,Label,Tk,Frame,Scrollbar,mainloop,Entry,Text,Menu,messagebox
from tkinter.ttk       import Treeview,Notebook,Separator

'''
   SIMPLE TODO APP
'''

logging.basicConfig(filename="debug.log",level=logging.DEBUG,format="%(created)f:%(funcName)s:%(message)s")
logger=logging.getLogger()

class Item:
    uuid_:str=None
    created:str=None
    title:str=None
    desc:str=None

    def __init__(self,uuid_,created,title,desc):
        self.created = created or date.today().isoformat()
        self.uuid_ = uuid_ or str(uuid.uuid4())
        self.title = title
        self.desc = desc
    
    def __repr__(self):
        return f'<Item(uuid_="{self.uuid_}",created="{self.created}",title="{self.title}",desc="{self.desc}">'

    def update(self,title,desc):
            self.title=title
            self.desc=desc

    def get(self):
        return self.__dict__

    def equal(self,item):
        return ( self.uuid_ == item.uuid_  )
"""
  TODO : json is portable but does not serialize objects
"""
class JsonStore:
    store="storarge.json"

    def __init__(self):
        try:
            with open(self.store,"r") as fs:
                self.items = [  Item(**d)  for  d  in  json.load( fs ) ]
        except FileNotFoundError as ex:
            self.items=[]
        except Exception as ex:
            logger.critical(ex)
            sys.exit(1)
        
    def findItem(self,item):
        for x in self.items:
            if x.equal(item):
                return x
        return None
    
    def findItemByUUID(self,uuid_):
        for i in self.items:
            if i.uuid_ == uuid_:
                return i
        return None


    def addItem(self,item):
        if not self.findItem(item):
            self.items.append(item)
        else:
            logger.debug(f'Already added {item}')

    def deleteItem(self,item):
        try:
            self.items.remove(item)
        except ValueError as ex:
            logger.error(ex)

    def getItems(self):
        return self.items

    def save(self):
        try:
            with open(self.store,"w") as fs:
                json.dump([ x.__dict__ for x in self.items] ,fs)

        except Exception as ex:
            logger.critical(ex)
            sys.exit(1)


class ContextMenu(Frame):

    def __init__(self,root,store,treeview):
        super().__init__(master=root)
        self.root=root
        self.treeview=treeview
        self.store=store
        self.menu=Menu(self.treeview,tearoff=0)
        self.menu.add_command(label="Delete",command=self.delete)
        self.menu.add_command(label="Edit",command=self.edit)

        self.treeview.bind('<Button-3>',func=self.popup)
        self.treeview.bind('<Button-1>',func=self.popdown)
        self.treeview.bind('<Double-Button-1>',func=self.edit)

    def popdown(self,event):
        self.menu.grab_release()
        self.menu.unpost()
    
    def popup(self,event):
        #  UGLY? direct access to data members
        if self.treeview.master.isempty():
            return
        self.treeview.event_generate('<Button-1>',x=event.x,y=event.y)
        try:
            self.menu.tk_popup(event.x_root,event.y_root,0)
        finally:
            self.menu.grab_release()
        
    def delete(self):
        row=self.treeview.item(self.treeview.selection())['values']
        response=messagebox.askyesno(title="Remove",message=row[2])
        if not response:
            return # OK , leave it

        #item=self.store.findItem(Item(uuid_=row[0],created=None,title=None,desc=None))
        item=self.store.findItemByUUID(row[0])
        if not item:
            logger.debug(f'no item found in store : {row}')
            return
        self.store.deleteItem(item)
        self.treeview.master.update()

    def edit(self,event=None):
        row=self.treeview.item(self.treeview.selection())['values']
        if not row or not row[0] or not row[1]:
            logger.debug("no row found")
            return
        item=self.store.findItem(Item(uuid_=row[0],created=None,title=None,desc=None))
        if not item:
            logger.debug("no item found "+str(row))
            return
        self.root.add_tab(child=EditTodo(root=self.root,store=self.store,item=item),title=item.title[:18])
        self.root.layout.select(2)

class ListTodo(Frame):

    def __init__(self,root,store):
        super(ListTodo,self).__init__(master=root,borderwidth=10)
        self.root=root
        self.store=store
        self.treeview=Treeview(self,columns=['uuid','created','title'])
        self.treeview.configure(show='headings')
        self.treeview['displaycolumns']=('created','title')
        self.treeview.heading('#1',text='Creado')
        self.treeview.column('#1',width='90',stretch='no')
        self.treeview.heading('#2',text='Titulo')
        self.treeview.column('#2',width='260',stretch='yes')

        self.scrollbar=Scrollbar(self,orient='vertical')
        self.scrollbar.configure(command=self.treeview.yview)
        self.treeview.configure(yscroll=self.scrollbar.set,selectmode='browse')

        self.scrollbar.pack(fill='y',side='right')

        self.contextmenu=ContextMenu(root=root,store=store,treeview=self.treeview)
        
        self.update(None)
        #self.bind('<Expose>',self.update)
        self.treeview.pack(fill='both',expand='yes')

        self.pack(fill='both',expand='yes')
        
        #self.menu=Menu(self.treeview,tearoff=0)
        #self.menu.add_command(label="Delete",command=self.delete)
        #self.menu.add_command(label="Edit",command=self.edit)

    def update(self,event=None):
        self.clear()
        for x in self.store.items:
            self.treeview.insert('','end',values=(x.uuid_,x.created,x.title))
        #self.event_generate('<FocusIn>')
        self.focus_force()

    def clear(self,event=None):
        for x in self.treeview.get_children():
            self.treeview.delete(x)

    def isempty(self):
        return self.treeview.get_children() == ()


class AbstractTodo(Frame):

    def __init__(self,root,button_title,store):
        super(AbstractTodo,self).__init__(master=root,borderwidth=10) 
        self.store=store
        self.root=root
        self.msgerr=list()
        Separator(master=self).pack(pady=5)
        Label(self,text='Titulo').pack(anchor='w')
        self.title=Entry(self)
        self.title.pack(fill='x')
        
        Separator(master=self).pack(pady=5)
        
        Label(self,text='Descripcion').pack(anchor='w')

        _wrap=Frame(master=self)
        self.desc=Text(master=_wrap,width=10,height=10)
        self.desc.pack(fill='both',expand=1,side='left')
        #self.desc.grid(row=0,column=0)   # Grid doesn't have fill property
        scrollbar=Scrollbar(master=_wrap,orient='vertical',command=self.desc.yview)
        self.desc.configure(yscroll=scrollbar.set)
        scrollbar.pack(fill='y',side='right')
        #scrollbar.grid(row=0,column=1,sticky='nsew')

        #self.add_button=Button(master=_wrap,text="Agregar")
        #self.add_button.grid(row=1,column=0,columnspan=2,sticky='nsew')

        _wrap.pack(fill='both',expand='yes')
    
        self.add_button=Button(master=self,text=button_title)
        self.add_button.bind('<ButtonRelease>',func=self.on_submit)
        #self.bind('<Unmap>',self.clear)
        self.add_button.pack(pady=10)

        self.pack(fill='both')
            
    def on_submit(self,event=None):
        raise NotImplementedError("Unimplemented method or abstract instance")
        

    def validate(self):
        vals=self.getValues()

        if not vals['title'] :
            self.msgerr.append("Title is empty")
        if not vals['desc']:
            self.msgerr.append("Descripcion is empty")
        if len(vals['title']) >20:
            self.msgerr.append("Title excess 20 chars)")
        if len(vals['desc']) > 16777216:
            self.msgerr.append("Description excess 16777216 chars")


        return len(self.msgerr) == 0

    def getValues(self):
        return dict(title=self.title.get().strip(),desc=self.desc.get(1.0,'end-1c').strip())

    def clear(self,event=None):
        self.title.delete(0,'end')
        self.desc.delete(1.0,'end-1c' )

    def set(self,item):
        self.clear()
        self.title.insert(0,item.title)
        self.desc.insert(1.0,item.desc)

class AddTodo(AbstractTodo):

    def __init__(self,root,store):
        super(AddTodo,self).__init__(root=root,store=store,button_title="Agregar") 
            
    def on_submit(self,event=None):
        if not self.validate():
            messagebox.showerror(title="Error",message="\n".join(self.msgerr))
            self.msgerr.clear()
            return
    
        data=self.getValues()

        new=Item(uuid_=None,created=None,title=data['title'],desc=data['desc'])

        logger.debug(f'add={new}')
        self.store.addItem( new )
        self.clear()
        self.root.listtodo.update()
        self.root.layout.select(0)


class EditTodo(AbstractTodo):

    def __init__(self,root,store,item):
        super(EditTodo,self).__init__(root=root,button_title="Modificar",store=store)
        self.bind('<Unmap>',lambda e:self.destroy())
        self.saved_item=item
        self.set(item)

    def on_submit(self,event=None):
        if not self.validate():
            messagebox.showerror(title="Error",message="\n".join(self.msgerr))
            self.msgerr.clear()
            return

        values=self.getValues()

        found=self.store.findItem( self.saved_item)
        if not found:
            logger.error(f'item no found uuid={self.saved_item}')
            return
        
        found.update(title=values['title'],desc=values['desc'])
        logger.debug(f'old={self.saved_item} , new={found}')

        #new_item = Item(uuid_=self.item.uuid_,created=self.item.created,title=values['title'],desc=values['desc'],created=item.created)

        #self.store.deleteItem(item)
        #self.store.addItem(new_item)

        self.root.listtodo.update()
        self.root.layout.select(0)
        #self.clear()

class Application (Frame):
    def __init__(self,root):
        super(Application,self).__init__(master=root)
        self.root=root
        self.store=JsonStore()

        self.layout=Notebook(master=self)
        self.listtodo=ListTodo(root=self,store=self.store)

        self.addtodo=AddTodo(root=self,store=self.store)
        self.layout.add(child=self.listtodo,text='Listado')
        self.layout.add(child=self.addtodo,text="Agregar")
        
        self.addtodo.bind('<FocusOut>',self.listtodo.update)
        self.layout.pack(fill='both',expand='yes')
    
        self.bind('<Destroy>',lambda e:self.store.save)
        
        self.pack(fill='both',expand='yes')

    def exit(self):
        self.store.save()
        self.root.destroy()

    def add_tab(self,child,title):
        self.layout.add(child=child,text=title)


def main():
    root=Tk()
    root.configure(borderwidth=10)
    #root.wm_geometry('400x300')
    app=Application(root)
    #root.protocol('WM_DELETE_WINDOW',app.exit)
    
    root.wm_title('Todo')
    mainloop()


if __name__ == '__main__':
    main()
